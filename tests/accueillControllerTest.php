<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request; 
use Twig\Environment;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * @Route("/accueil")
 */

class AccueilController extends AbstractController
{
    /**
     * @Route("/{page}", name="accueil", requirements={"page" = "\d+"}, defaults={"page" = 1})
     */
    public function index()
    {
        $url = $this->generateUrl(
            'acc_view', // 1er argument : le nom de la route
            ['id' => 5]       // 2e argument : les paramètres
        );


        return $this->render('accueil/index.html.twig', [
            'controller_name' => 'AccueilController',
            'url' => $url
        ]);
    }

    /**
     * @Route("/view/{id}", name="acc_view", requirements={
     * "id" = "\d+"
     * })
     */

    public function view($id, Request $request)
    {
        $tag = $request->query->get('tag');

    return $this->render('accueil/view.html.twig', [
        'controller_name' => 'Accueil->view',
        'tag' => $tag,
        'id' => $id
        
    ]);
    }

        // On injecte la session avec SessionInterface
    public function viewAction($id, SessionInterface $session)
    {

        $session = new Session();
        $session->start();
        // On récupère le contenu de la variable userId
        $userId = $session->get('userId');

        // On définit une nouvelle valeur pour cette variable userId
        $session->set('userId', 91);

        // On n'oublie pas de renvoyer une réponse
    return new Response("<body>Je suis une page de test, je n'ai rien à dire</body>");
    }

    /**
     * @Route("/add", name="add")
     */

    public function add()
    {
                // Bien sûr, cette méthode devra réellement ajouter l'annonce

        // Mais faisons comme si c'était le cas
        $this->addFlash('info', 'Annonce bien enregistrée');

        // Le « flashBag » est ce qui contient les messages flash dans la session
        // Il peut bien sûr contenir plusieurs messages :
        $this->addFlash('info', 'Oui oui, elle est bien enregistrée !');

        // Puis on redirige vers la page de visualisation de cette annonce
        return $this->redirectToRoute('acc_view', ['id' => 5]);
    
    }

    /**
     * @Route("/viewSlug/{year}/{slug}.{format}", name="view_slug", requirements={
     * "year" = "\d{4}",
     * "format" = "html|xml"
     * })
     */

    public function viewSlug($slug, $year, $format)
    {

    return new Response(
        "On pourrait afficher l'annonce correspondant au
        slug '".$slug."', crée en '".$year."' et au format'".$format."'."
    );
    }

    /**
     * @Route("/edit/{id}", name="acc_edit", requirements={"id" = "\d+"})
     */
    public function edit($id)
    {
      // Créons nous-mêmes la réponse en JSON, grâce à la fonction json_encode()
    $response = new Response(json_encode(['id' => $id]));

    // Ici, nous définissons le Content-Type pour dire au navigateur
    // que l'on renvoie du JSON et non du HTML
    $response->headers->set('Content-Type', 'application/json');

    return $response;
    }

    /**
     * @Route("/delete/{id}", name="acc_delete", requirements={"id" = "\d+"})
     */
    public function delete($id)
    {
        return new Response(
            "Delete function"
           
        );
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request; 
use Twig\Environment;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * @Route("/accueil")
 */

class AccueilController extends AbstractController
{


    /**
     * @Route("/{page}", name="accueil", requirements={"page" = "\d+"}, defaults={"page" = 1})
     */
    public function index($page)
    {

         // Notre liste d'annonce en dur
    $listAdverts = array(
        array(
          'title'   => 'Recherche développpeur Symfony',
          'id'      => 1,
          'author'  => 'Alexandre',
          'content' => 'Nous recherchons un développeur Symfony débutant sur Lyon. Blabla…',
          'date'    => new \Datetime()),
        array(
          'title'   => 'Mission de webmaster',
          'id'      => 2,
          'author'  => 'Hugo',
          'content' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
          'date'    => new \Datetime()),
        array(
          'title'   => 'Offre de stage webdesigner',
          'id'      => 3,
          'author'  => 'Mathieu',
          'content' => 'Nous proposons un poste pour webdesigner. Blabla…',
          'date'    => new \Datetime())
      );

        if ($page < 1) {
            // On déclenche une exception NotFoundHttpException, cela va afficher
            // une page d'erreur 404 (qu'on pourra personnaliser plus tard d'ailleurs)
            throw $this->createNotFoundException('Page "'.$page.'" inexistante.');


            //$response = new Response();
            //$response->setContent("Ceci est une page d'erreur 404");
            //$response->setStatusCode(Response::HTTP_NOT_FOUND);
            //return $response;
          }


        $url = $this->generateUrl(
            'acc_view', // 1er argument : le nom de la route
            ['id' => 5]       // 2e argument : les paramètres
        );
       
        return $this->render('accueil/index.html.twig', [
            'name' => 'Verdade Accueil',
            'url' => $url,
            'listAdverts' => $listAdverts
        ]);
    }

    /**
     * @Route("/view/{id}", name="acc_view", requirements={
     * "id" = "\d+"
     * })
     */

    public function view($id, Request $request)
    {
      
    $advert = array(
        'title'   => 'Recherche développpeur Symfony2',
        'id'      => $id,
        'author'  => 'Alexandre',
        'content' => 'Nous recherchons un développeur Symfony2 débutant sur Lyon. Blabla…',
        'date'    => new \Datetime()
      );
  
      return $this->render('accueil/view.html.twig', array(
        'advert' => $advert
      ));


    }

        // On injecte la session avec SessionInterface
    public function viewAction($id, SessionInterface $session)
    {

        $session = new Session();
        $session->start();
        // On récupère le contenu de la variable userId
        $userId = $session->get('userId');

        // On définit une nouvelle valeur pour cette variable userId
        $session->set('userId', 91);

        // On n'oublie pas de renvoyer une réponse
    return new Response("<body>Je suis une page de test, je n'ai rien à dire</body>");
    }

    /**
     * @Route("/add", name="add")
     */

    public function add(Request $request)
    {

    // La gestion d'un formulaire est particulière, mais l'idée est la suivante :

    // Si la requête est en POST, c'est que le visiteur a soumis le formulaire
    if ($request->isMethod('POST')) {
        // Ici, on s'occupera de la création et de la gestion du formulaire
  
        $this->addFlash('notice', 'Annonce bien enregistrée.');
  
        // Puis on redirige vers la page de visualisation de cettte annonce
        return $this->redirectToRoute('acc_view', ['id' => 5]);
      }
        
      // Si on n'est pas en POST, alors on affiche le formulaire
        return $this->render('accueil/add.html.twig');
    
    }

    /**
     * @Route("/viewSlug/{year}/{slug}.{format}", name="view_slug", requirements={
     * "year" = "\d{4}",
     * "format" = "html|xml"
     * })
     */

    public function viewSlug($slug, $year, $format)
    {

    return new Response(
        "On pourrait afficher l'annonce correspondant au
        slug '".$slug."', crée en '".$year."' et au format'".$format."'."
    );
    }

    /**
     * @Route("/edit/{id}", name="acc_edit", requirements={"id" = "\d+"})
     */
    public function edit($id, Request $request)
    {
        $advert = array(
            'title'   => 'Recherche développpeur Symfony',
            'id'      => $id,
            'author'  => 'Alexandre',
            'content' => 'Nous recherchons un développeur Symfony débutant sur Lyon. Blabla…',
            'date'    => new \Datetime()
          );
      
          return $this->render('accueil/edit.html.twig', array(
            'advert' => $advert
          ));
    }

    /**
     * @Route("/delete/{id}", name="acc_delete", requirements={"id" = "\d+"})
     */
    public function delete($id)
    {

     return $this->render('accueil/delete.html.twig');

    }

      /**
     * @Route("/menu", name="acc_menu")
     */

    public function menu($max = 3)
    {
      // On fixe en dur une liste ici, bien entendu par la suite
      // on la récupérera depuis la BDD !
      $listAdverts = array(
        array('id' => 2, 'title' => 'Recherche développeur Symfony'),
        array('id' => 5, 'title' => 'Mission de webmaster'),
        array('id' => 9, 'title' => 'Offre de stage webdesigner')
      );
  
      return $this->render('accueil/menu.html.twig', array(
        // Tout l'intérêt est ici : le contrôleur passe
        // les variables nécessaires au template !
        'listAdverts' => $listAdverts
      ));
    }

}
